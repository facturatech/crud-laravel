<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\http\Controllers\UserController;

Route::get('/', function () {
    return view('usuarios.listar');
});

Route::get('/form','UserController@userform');
Route::post('/save','UserController@save')->name('save');