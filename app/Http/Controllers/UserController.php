<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;

class UserController extends Controller
{
    //Formulario de Usuario
 public function userform(){
     return view('usuarios.userform');

 }

    //Guardar usuarios

    public function save(Request $request){


        $validator = $this->validate($request,[
            'nombre'=> 'required|string|max:255',
            'email'=> 'required|string|max:225|email|unique:usuarios'
        ]);


        $userdata = request()->except('_token');

        Usuario::insert($userdata);

        return back()->with('usuarioGuardado','Usuario guardado');
    }


}
